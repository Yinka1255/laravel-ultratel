<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//General Routes
//Route::get('/', 'LoginsController@login');
// Route::get('login', function () {
//     return view('login');
// });
//Route::get('/login', 'LoginsController@login');
//Route::get('/logout', 'LoginsController@logout');
Route::post('/auth', 'LoginsController@auth');

Route::post('/createBucket', 'BucketsController@createBucket');
Route::post('/updateBucket', 'BucketsController@updateBucket');
Route::get('/buckets', 'BucketsController@buckets');
Route::get('/bucket/{id}', 'BucketsController@bucket');

Route::post('/addFilesToBucket', 'FilesController@addFilesToBucket');
Route::post('/deleteFilesFromBucket', 'FilesController@deleteFilesFromBucket');
Route::get('/files', 'FilesController@buckets');
Route::get('/file/{id}', 'FilesController@bucket');

//Route::get('dashboard', 'UsersController@dashboard')->middleware('auth');


?>