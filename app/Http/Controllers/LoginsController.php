<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Vendor;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class LoginsController extends Controller
{

    /**
	 * Handles authentication attempt
	 *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function login(Request $request){
        return view('login');
    }
    public function auth(Request $request){

    	$username = $request->input('username');

        $password = $request->input('password');

    	if (Auth::attempt(['username' => $username, 'password' => $password])){

            $user = Auth::user();

            if($user->status == 1){
                
                return response()->json(['success' => 'Authentication was successfull'],200);
            }   
            if($user->status == 0){
                return response()->json(['error' => 'Thank you, Kindly check your email to activate your account'],200);
            }   
            if($user->status == 2){
                return response()->json(['error' => 'Sorry! your account has been deactivated'],200);
            }   

        }else{		
            return response()->json(['error' => 'Sorry! Kindly provide a valid login details'],200);
        }

    }
}
