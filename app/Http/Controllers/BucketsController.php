<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Session;
use Redirect;
use App\User;
use App\Bucket;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Mail;

class BucketsController extends Controller{

    public function createBucket(Request $request){

        $bucket = new Bucket;
    	$bucket->name = $request->input('name');
        $bucket->status = 1;

        if($bucket->save()){
            File::makeDirectory(public_path()."/buckets/".$bucket->name);
            return response()->json(['success' => 'Thank you, Bucket created successfully'],200);
        }else{
            return response()->json(['error' => 'Sorry! A server error occured.'],200);
        }
    }

    public function updateBucket(Request $request){
        $bucketId = $request->input('bucketId');
        $bucket = Bucket::where("id", $bucketId)->first();
        $oldName = $bucket->name;
    	$bucket->name = $request->input('name');
        $bucket->status = 1;
        if($bucket->save()){
            Storage::move(public_path()."buckets/".$oldName, public_path()."buckets/".$bucket->name);
            return response()->json(['success' => 'Thank you, Bucket updated successfully'],200);
        }else{
            return response()->json(['error' => 'Sorry! A server error occured.'],200);
        }
    }


    public function getBuckets(){
        $buckets = Bucket::all();
        return response()->json(['response' => true, 'buckets'=>$buckets],200);
    }

    public function getBucket($bucketId){
        $bucket = Bucket::where("id", $bucketId)->first();
        return response()->json(['response' => true, 'bucket'=>$bucket],200);
    }
   
    
}
