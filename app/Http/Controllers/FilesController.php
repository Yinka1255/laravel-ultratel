<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Session;
use Redirect;
use App\User;
use App\File;
use App\BucketFile;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Mail;

class FilesController extends Controller{

    public function getFiles(){
        $files = File::all();
        return response()->json(['response' => true, 'files'=>$files],200);
    }

    public function getFile($fileId){
        $file = File::where("id", $fileId)->first();
        return response()->json(['response' => true, 'file'=>$file],200);
    }

    public function addFilesToBucket(Request $request){
        $bucketId = $request->input('bucketId');
        $bucket = Bucket::where("id", $bucketId)->first();
        $bucketFile = new BucketFile;
    	$bucketFile->name = $request->input('name');
        $bucketFile->status = 1;

        $file = $request->file('file');
        $fileName  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path()."/buckets/".$bucket->name;
        $file->move($path, $fileName);
        $bucketFile->file = $fileName;

        if($bucketFile->save()){
            return response()->json(['success' => 'Thank you, File has been added to bucket successfully'],200);
        }else{
            return response()->json(['error' => 'Sorry! A server error occured.'],200);
        }
    }

    public function deleteFilesFromBucket(Request $request){
        $bucketFileId = $request->input('bucketFileId');
        $bucketFile = BucketFile::where("id", $bucketFileId)->first();
        $bucketFile->status = 0; //status zero means item has been deleted

        if($bucketFile->save()){
            return response()->json(['success' => 'Thank you, file has been deleted from bucket successfully'],200);
        }else{
            return response()->json(['error' => 'Sorry! A server error occured.'],200);
        }
    }
    
}
